---
layout: markdown_page
title: "Developer Relations"
---
# Welcome to the Developer Relations Handbook  

The Developer Relations organization includes Developer Advocacy, Technical Writing, and Field Marketing.   

[Up one level to the Marketing Handbook](/handbook/marketing/)

Developer Relations Handbooks:  

- [Developer Advocacy](/handbook/marketing/developer-relations/developer-advocacy/)  
- [Field Marketing](/handbook/marketing/developer-relations/field-marketing/)  
- [Technical Writing](/handbook/marketing/developer-relations/technical-writing/)  
